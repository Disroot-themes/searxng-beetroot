# Searxng_beetroot

Disroot theme for Searxng.

## How to use
To use this theme, `git clone` it on your computer.

Create a `beetroot` folder in `/var/www/searx/searx/static/themes` and in `/var/www/searx/searx/templates`.

Then, copy the content of `beetroot_static` in `/var/www/searx/searx/static/themes/beetroot` and the content of `beetroot_template` in `/var/www/searx/searx/templates/beetroot`.

Enable the beetroot them by editing `/var/www/searx/searx/settings.yml` and changing:
```
default_theme : beetroot
```

Restart searx: `service uwsgi restart`


## How to edit/change this theme
Simply clone the theme to your machine.
All changes done to the the templates do not require compilation, only searxng reload. 
When changing static elements (css, images etc), edit files in `src/`. Once done run `grunt` to compile. To do so, in your running searxng instance, your first need to:
```
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
source ~/.profile or ~/.bashrc
nvm install 18.19.0 # or whatever version you want
npm install -g npm
cd searx/static/themes/beetroot
npm install
npm install -g grunt-cli
```
Then you can run `grunt` from `searx/static/themes/beetroot`. Those compiled files can be found in `searx/static/themes/beetroot/css` 

If you want to test in live mode all your changes to the `beetroot` theme, you need working searx instance with your changes pulled into it. You can then either select the theme from UI or run searx with theme by running `LIVE_THEME=beetroot make run` from searx-src folder and edit.

## Screenshots

### Light mode
![Light mode](img/Light01.png)
![Light mode](img/Light02.png)
![Light mode](img/Light03.png)

### Dark mode
![Dark mode](img/Dark01.png)
![Dark mode](img/Dark02.png)
![Dark mode](img/Dark03.png)
